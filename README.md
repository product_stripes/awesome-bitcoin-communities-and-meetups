# awesome-bitcoin-communities-and-meetups

A curated list of bitcoin communities and meetups worldwide, to complement [awesome-bitcoin](https://github.com/igorbarinov/awesome-bitcoin).

## Contributing

- Fork the project / repo
- Create a new branch from main
- Make changes (add a new community or meetup)
- Commit changes and push branch to your repo
- Open a pull request to pull your brance into the main project
- Project owners will review, merge or close the PR

## Project status
20240122 - Just getting started

## License
[LICENSE](https://gitlab.com/product_stripes/awesome-bitcoin-communities-and-meetups/-/blob/main/LICENSE)